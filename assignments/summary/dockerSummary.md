

### **What is Docker and why it is used ?**

- Docker is a tool designed to make it easier to create, deploy, and run applications by using containers.
-  Containers allow a developer to package up an application with all of the parts it needs, such as libraries and other dependencies, and deploy it as one package.
  
  ---

### **Docker Architecture**

.....

![](https://wiki.aquasec.com/download/attachments/2854889/Docker_Architecture.png?version=1&modificationDate=1520172700553&api=v2)



 The Docker client talks to the Docker daemon, which does the heavy lifting of building, running, and distributing your Docker containers. The Docker client and daemon can run on the same system, or you can connect a Docker client to a remote Docker daemon.

---


### **Docker Terminology**

1. Docker Image
2. Docker Container
3. Docker Hub


#### **Docker Image**

- In Docker, everything is based on Images. An image is a combination of a file system and parameters.
- The image  can be deployed to any Docker environment and as a container.
- A Docker image is contains everything needed to run an applications as a container. 

    - Code
    - Runtime
    - Libraries
    - envirinments variables
    - configuration files
  


#### **Docker container**

`Containers` are instances of Docker images that can be run using the Docker run command. The basic purpose of Docker is to run containers.


#### **Docker Hub**


![Docker Hub](https://miro.medium.com/max/826/1*ttU6oMoZztKk2kjJid6PuQ.png)



- `Docker Hub` is a registry service on the cloud that allows to download Docker images. We can also upload own Docker built images to Docker hub. 


---


### **Install Docker on Ubuntu 16.04**

1. Uninstall the older version of docker if is already installed.
   
   `$ sudo apt-get remove docker docker-engine docker.io containerd runc`

  

2. Installing CE (Community Docker Engine).
 


       $ sudo apt-get update
       $ sudo apt-get install \
          apt-transport-https \
          ca-certificates \
          curl \
          gnupg-agent \
          software-properties-common
       $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
       $ sudo apt-key fingerprint 0EBFCD88
       $ sudo add-apt-repository \
           "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
           $(lsb_release -cs) \
           stable nightly test"
        $ sudo apt-get update
        $ sudo apt-get install docker-ce docker-ce-cli containerd.io

        // Check if docker is successfully installed in your system
          
        $ sudo docker run hello-world

---


### **Docker Basic Commands**

1. **docker ps**
   
   The docker ps command allows us to view all the containers that are running on the Docker Host.

2. **docker start**

    Start one or more stopped containers.

3. **docker stop**

    This command stops any running container.

4. **docker run**

    This command creates containers from docker images.


5. **docker rm**

    This command deletes the containers.

    ---


### **Common Operations on Dockers**

1. Download/pull the docker images that you want to work with.
2. Copy your code inside the docker
3. Access docker terminal
4. Install and additional required dependencies
5. Compile and Run the Code inside docker
6. Document steps to run your program in README.md file
7. Commit the changes done to the docker.
8. Push docker image to the docker-hub and share repository with people who want to try your code.


 ---   