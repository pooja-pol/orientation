
## **What is Git ?**

Git is the most commonly used `version control system` today and is quickly becoming the standard for version control.

## **What is Gitlab ?**

GitLab is a web-based tool that manage Git-repositories.


![Git VCS](https://miro.medium.com/max/2292/1*m-5KwN8hE9GgOtjaX9W9Qw.png)

---
## **Basic terms in Gitlab** 


### **Reposotory**

 A repository can be a place where multiple databases or files are located. 

 Git has two sorts of repositories: one called `local`, another called `remote`. 

 Local repository is stored on our loacal machine and Remote repository is stored on an internal server.

 
 ### **Fork**

 A `fork` is a copy of original repository. The repository remains on your Gitlab account and allows you to freely experiment without affecting the original project.

 ### **Clone**

 `Clone` is use for making exact copy of online repository on lacal machine.
  
  ### **commit**

  Saving work by taking pictures/snapshots of existing file.

  ### **Push**

  This think is used to upload local repository content to a remote repository. 

  ### **Pull**

  A `pull` grabs any changes from the Gitlab repository and merges them into your local repository.
  
  ---

  ### **Basic Git Work Flow**

_Git Intenals_  

1. `Workspace` : All the changes you make via Editor(s) (gedit,
   notepad, vim, nano) is done in this tree of repository.

2. `Staging` : All the staged files go into this tree of your 
   repository.

3. `Local Repository` : the repositiry stored inti your local 
   machine. 


4. `Remote Repository` : This is the copy of your Local 
   Repository  but is stored in some server on the Internet. 

   

  ![WorkFlow](https://static.wixstatic.com/media/7a237f_368be22be5234eeb88b3caf0219d8e36~mv2.png/v1/fill/w_695,h_346,al_c,q_85,usm_0.66_1.00_0.01/gitworkflow_PNG.webp)


---


  ### **Basic Git commands**

  
  - ` $ git init `
     
    This command turns a directory into an empty Git repository.

  - ` $ git add `

     _Adds files_ in the to the staging area for Git. Before a file is available to commit to a repository, the file needs to be added to the Git index (staging area). 

   - ` $ git status `
   
     This command returns the current state of the repository.


   - ` $ git commit `
 
     The _commit_  command is used to save your changes to the local repository. 

   - ` $ git push `

     The _git push_ command is used to upload local repository content to a remote repository.

   - ` $ git pull `

      The _git pull_ command is used to fetch and download content from a remote repository and immediately update the local repository to match that content.

   - ` $ git config `

      With this command, there are many configurations and settings possible. Two important settings are user.email and user. name

   - ` $ git branch `

      To determine what _branch_ the local repository is on, add a new branch, or delete a branch.

   - ` $ git merge`

      Integrate branches together.  _git merge_ combines the changes from one branch to another branch. 

      ---


        